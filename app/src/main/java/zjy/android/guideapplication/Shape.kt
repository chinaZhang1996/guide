package zjy.android.guideapplication

enum class Shape {
    ROUND_RECTANGLE, RECTANGLE, CIRCLE, OVAL
}