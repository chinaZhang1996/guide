package zjy.android.guideapplication

import java.util.*

class GuideGroup(guide: ICustomGuide) {

    private val guides: MutableList<ICustomGuide> = ArrayList()

    fun add(guide: ICustomGuide) {
        guides.add(guide)
    }

    fun getGuides(): List<ICustomGuide> {
        return guides
    }

    init {
        guides.add(guide)
    }
}