package zjy.android.guideapplication

/**
 * @Create zhang
 * @DATE 2021/6/15 0015
 * @DESC
 */
class Padding internal constructor(val left: Float, val top: Float, val right: Float, val bottom: Float) {

    companion object {
        fun all(padding: Float): Padding {
            return Padding(padding, padding, padding, padding)
        }

        fun only(left: Float, top: Float, right: Float, bottom: Float): Padding {
            return Padding(left, top, right, bottom)
        }
    }

}