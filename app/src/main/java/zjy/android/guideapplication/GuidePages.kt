package zjy.android.guideapplication

import android.app.Activity
import java.util.*

/**
 * @Create zhang
 * @DATE 2021/6/9 0009
 * @DESC
 */
class GuidePages private constructor(private val activity: Activity) {
    private val groups: ArrayList<GuideGroup> = ArrayList()
    private var currGroup: GuideGroup? = null

    fun addPage(guide: ICustomGuide): GuidePages {
        if (guide is GuideItem) guide.initHighlight(activity)
        return this.apply {
            GuideGroup(guide).also {
                groups.add(it)
                currGroup = it
            }
        }
    }

    fun with(guide: ICustomGuide): GuidePages {
        return this.apply {
            currGroup?.add(guide) ?: addPage(guide)
        }
    }

    fun start() {
        activity.window.decorView.let {
            it.post {
                GuidePopupWindow(activity, groups).showAsDropDown(it)
            }
        }
    }

    companion object {
        fun init(activity: Activity): GuidePages {
            return GuidePages(activity)
        }
    }

}