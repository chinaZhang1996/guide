package zjy.android.guideapplication

enum class Direction {
    LEFT, RIGHT, TOP, BOTTOM
}