package zjy.android.guideapplication

import android.app.Activity
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import androidx.core.graphics.ColorUtils

class GuidePopupWindow(private val activity: Activity, private val groups: List<GuideGroup>) : PopupWindow() {

    private var originalColor = 0

    private fun init() {
        isFocusable = true
        isOutsideTouchable = false
        setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        isTouchable = true
        animationStyle = 0

        contentView = LayoutInflater.from(activity).inflate(R.layout.activity_guide, null)
                .apply {
                    findViewById<GuideView>(R.id.guide_view).apply {
                        setGroups(groups)
                        onFinishListener = object : GuideView.OnFinishListener {
                            override fun onFinish() {
                                dismiss()
                            }
                        }
                    }
                }
        width = ViewGroup.LayoutParams.MATCH_PARENT
        height = ViewGroup.LayoutParams.MATCH_PARENT

        originalColor = activity.window.statusBarColor
    }

    override fun showAsDropDown(anchor: View) {
        super.showAsDropDown(anchor)
        val shadeColor = activity.resources.getColor(R.color.alpha)
        activity.window.statusBarColor = ColorUtils.compositeColors(shadeColor, originalColor)
    }

    override fun dismiss() {
        activity.window.statusBarColor = originalColor
        super.dismiss()
    }

    init {
        init()
    }
}