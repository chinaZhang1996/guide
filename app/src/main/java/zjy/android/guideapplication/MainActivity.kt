package zjy.android.guideapplication

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import zjy.android.guideapplication.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        val listener = View.OnClickListener {
            show()
        }
        binding.center.setOnClickListener(listener)
        binding.leftBottom.setOnClickListener(listener)
        binding.leftTop.setOnClickListener(listener)
        binding.centerLeft.setOnClickListener(listener)
        binding.rightBottom.setOnClickListener(listener)
        binding.rightTop.setOnClickListener(listener)
        binding.centerRight.setOnClickListener(listener)
        binding.centerTop.setOnClickListener(listener)
        binding.centerBottom.setOnClickListener(listener)
        show()
    }

    private fun show() {
        val item = GuideItem()
                .setFinishClickId(R.id.skip)
                .setHintResId(R.layout.layout_skip)
        GuidePages.init(this)
                .addPage(GuideItem(target = binding.centerTop,
                        hintResId = R.layout.tip_arrows_top,
                        offsetX = -100,
                        offsetY = 50,
                        nextClickId = R.id.tips))
                .with(item)
                .with(GuideItem()
                        .setTarget(binding.centerBottom)
                        .setHintResId(R.layout.tip_arrows_bottom)
                        .setDirection(Direction.TOP)
                        .setNextClickId(R.id.tips)
                        .setShape(Shape.CIRCLE)
                        .setOffsetY(-50))
                .addPage(GuideItem()
                        .setTarget(binding.centerLeft)
                        .setHintResId(R.layout.tip_arrows_left)
                        .setOffsetX(-150)
                        .setOffsetY(310)
                        .setNextClickId(R.id.tips)
                        .setDirection(Direction.RIGHT))
                .with(item)
                .addPage(object : ICustomGuide {
                    override fun onDrawHighlight(canvas: Canvas, path: Path, paint: Paint) {
                        val path1 = Path()
                        path.addRoundRect(100f, 100f, 300f, 300f, 10f, 10f, Path.Direction.CW)
                        path1.addRoundRect(50f, 50f, 350f, 350f, 10f, 10f, Path.Direction.CW)
                        path.op(path1, Path.Op.XOR)
                        paint.color = Color.argb(25, 0, 0, 0)
                        canvas.drawPath(path, paint)
                        path.reset()
                        path.addPath(path1)
                    }

                    override fun onCreateGuideLayout(context: Context, next: Callback, finish: Callback): View? {
                        return null
                    }
                })
                .addPage(GuideItem()
                        .setTarget(binding.centerRight)
                        .setNextClickId(R.id.tips)
                        .setHintResId(R.layout.tip_arrows_right)
                        .setDirection(Direction.LEFT))
                .with(item)
                .start()
    }
}